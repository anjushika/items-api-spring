package com.classpath.items.controller;

import com.classpath.items.model.Item;
import com.classpath.items.service.ItemService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ItemController {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");

        ItemService itemService = applicationContext.getBean("itemService", ItemService.class);

        itemService.saveItem( Item.builder()
                                    .itemId(12)
                                    .itemPrice(23_000)
                                    .desc("A test Item")
                                    .name("IPad").build());



        //new Item(12, 23_000, "desc", "IPdad")
    }
}